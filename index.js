var express = require('express');
var app = express();
var server = require('http').createServer(app)
var io = require('socket.io').listen(server)
var fs = require('fs')
var path = require ("path");
server.listen(3000);

 
app.use(express.static(path.join(__dirname, 'temp')));

app.get('/',(req,res)=>{
    res.sendFile(__dirname + '/upload-video.html')
})
var Files = {};


io.sockets.on('connection', function (socket) {
    //Events will go here
    
        socket.on('Start', function (data) { //data contains the variables that we passed through in the html file
            var Name = data['Name'];
            Files[Name] = {  //Create a new Entry in The Files Variable
                FileSize : data['Size'],
                Data     : "",
                Downloaded : 0
            }
            var Place = 0;
            try{
                var Stat = fs.statSync('temp/' +  Name);
                if(Stat.isFile())
                {
                    Files[Name]['Downloaded'] = Stat.size;
                    Place = Stat.size / 524288;
                }
            }
            catch(er){} //It's a New File
            fs.open("temp/" + Name, "a", 0755, function(err, fd){
                if(err)
                {
                    console.log(err);
                }
                else
                {
                    Files[Name]['Handler'] = fd; //We store the file handler so we can write to it later
                    socket.emit('MoreData', { 'Place' : Place, Percent : 0 });
                }
            });
      });

      socket.on('Upload', function (data){
        var Name = data['Name'];
        Files[Name]['Downloaded'] += data['Data'].length;
        Files[Name]['Data'] += data['Data'];
        if(Files[Name]['Downloaded'] == Files[Name]['FileSize']) //If File is Fully Uploaded
        {
            fs.write(Files[Name]['Handler'], Files[Name]['Data'], null, 'Binary', function(err, Writen){
                let inp = fs.createReadStream("temp/" + Name);
                let out = fs.createWriteStream("video/" + Name);
                inp.pipe(out);
                inp.on('end', ()=>{
                    fs.unlink("temp/" + Name, () => { 
                        files = {};
                        var Place = Files[Name]['Downloaded'] / 524288;
                        var Percent = (Files[Name]['Downloaded'] / Files[Name]['FileSize']) * 100;
                        socket.emit('Done',{ 'Place' : Place, 'Percent' :  Percent , url : Name});
                    });
                })
            });
        }
        else if(Files[Name]['Data'].length > 10485760){ //If the Data Buffer reaches 10MB
            fs.write(Files[Name]['Handler'], Files[Name]['Data'], null, 'Binary', function(err, Writen){
                Files[Name]['Data'] = ""; //Reset The Buffer
                var Place = Files[Name]['Downloaded'] / 524288;
                var Percent = (Files[Name]['Downloaded'] / Files[Name]['FileSize']) * 100;
                socket.emit('MoreData', { 'Place' : Place, 'Percent' :  Percent});
            });
        }
        else
        {
            var Place = Files[Name]['Downloaded'] / 524288;


            console.log("Downloaded" ,Files[Name]['Downloaded'])
            console.log("Place" , Place)

            console.log( (Files[Name]['Downloaded']  / Files[Name]['FileSize']) * 100 )
            var Percent = (Files[Name]['Downloaded'] / Files[Name]['FileSize']) * 100;
            socket.emit('MoreData', { 'Place' : Place, 'Percent' :  Percent});
        }
    });



   





});